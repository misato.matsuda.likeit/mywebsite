﻿CREATE DATABASE mywebsite DEFAULT CHARACTER SET utf8;
USE mywebsite;
CREATE TABLE user(id int PRIMARY KEY AUTO_INCREMENT, loginId varchar(255) UNIQUE NOT NULL, password varchar(255) NOT NULL, userName varchar(255) NOT NULL, birthday date NOT NULL, postalCode varchar(255) NOT NULL, phoneNumber int NOT NULL);
CREATE TABLE buy(id int PRIMARY KEY AUTO_INCREMENT, userId int NOT NULL, deliveryId int NOT NULL, totalPrice int NOT NULL, buytime date NOT NULL);
CREATE TABLE delivery(id int PRIMARY KEY AUTO_INCREMENT, buyId int NOT NULL, method varchar(255) NOT NULL, price int NOT NULL);
CREATE TABLE buy_detail(id int PRIMARY KEY AUTO_INCREMENT, buyId int NOT NULL, productId int NOT NULL);
CREATE TABLE product(id int PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT NULL, description text NOT NULL, price int NOT NULL, charaName varchar(255) NOT NULL);
